DROP DATABASE IF EXISTS docker_ex1;

CREATE DATABASE IF NOT EXISTS docker_ex1;

USE docker_ex1;

CREATE TABLE proverbio (codProverbio SERIAL, testo VARCHAR(500));

INSERT INTO proverbio (testo) VALUES ('Non è tutto oro quel che luccica.'), 
('Morto un papa se ne fa un altro.'),
('Chi non risica non rosica.'),
('A buon intenditor, poche parole.'),
('Uomo avvisato mezzo salvato.'),
('Chi non lavora non mangia.'),
('Chi non ha testa, abbia buone gambe.'),
('Ognuno ha in casa sua il morto da piangere.'),
('Un padre campa cento figli e cento figli non campano un padre.'),
('Il gioco è bello quando dura poco.'),
('Tra moglie e marito non mettere il dito.'),
('Acqua cheta rompe i ponti.'),
('Vivi e lascia vivere.'),
('A caval donato non si guarda in bocca.'),
('L’erba del vicino è sempre più verde.'),
('L’appetito vien mangiando.'),
('Aiutati che Dio t’aiuta.'),
('Quando c’è la salute c’è tutto.'),
('Piove sempre sul bagnato.'),
('Ognuno ha la sua croce.'),
('Occhio non vede, cuore non duole.'),
('Ambasciator non porta pena.'),
('A pagare e morire c’è sempre tempo.'),
('A cavallo d’altri non si dice zoppo.'),
('Fino alla bara sempre s’impara.'),
('A chi batte forte, si apron le porte.'),
('Chi disprezza compra.'),
('Il lupo perde il pelo, ma non il vizio.'),
('Gli errori degli altri sono i nostri migliori maestri.');
